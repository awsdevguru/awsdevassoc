# DynamoDB Role Example
This example creates a basic DynamoDB table and loads 10 items into the table.  This code should be executed on an EC2 instance with a role assigned that permits interaction with DynamoDB.

## Install Deps
```
npm install 
```

## Run
```
node app.js
```
